# Annales du DNB de mathématiques depuis 2013

L'APMEP réalise depuis très longtemps la transcription des annales de DNB au format LaTeX. Les contributeurs de Coopmaths.fr ont souhaité utilisé ce travail avec un découpage et un classement exercice par exercice.

Sébastien Lozano a réalisé un script qui analyse les fichiers LaTeX des sujets pour les découper et créer une image utilisée dans le navigateur pour les aperçus.

La communauté a ensuite ajouté les mots clés pour intégrer ces exercices à coopmaths.fr/alea.

Ici, on garde les sources des annales des années passées et on organisera la transcription et la correction des futurs sujets.
